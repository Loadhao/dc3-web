import Vue from 'vue'

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import './element-variables.scss'

import locale from 'element-ui/lib/locale/lang/zh-CN'
import GeminiScrollbar from 'vue-gemini-scrollbar'
import baseCard from '@/components/card/base-card'
import titleCard from '@/components/card/title-card'
import Avue from '@smallwei/avue'
import '@smallwei/avue/lib/index.css'

Vue.use(Element, {locale});
Vue.use(Avue);
Vue.use(GeminiScrollbar);

Vue.component('base-card', baseCard);
Vue.component('title-card', titleCard);
