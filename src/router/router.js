import Vue from 'vue'
import VueRouter from 'vue-router'
import CommonRouter from './common/index'
import ViewsRouter from './views/index'
import NProgress from "nprogress";
import {getToken} from "@/util/auth";
import store from "@/store/store";
import {checkTokenValid} from "@/api/user";

Vue.use(VueRouter);

const router = new VueRouter({
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            if (from.meta.keepAlive) {
                from.meta.savedPosition = document.body.scrollTop;
            }
            return {
                x: 0,
                y: to.meta.savedPosition || 0
            }
        }
    },
    routes: []
});

router.beforeEach((to, from, next) => {
    NProgress.start();
    const meta = to.meta || {};
    if (meta.title) {
        document.title = to.meta.title
    }
    if (meta.isAuth !== true) {
        next();
    } else {
        const token = getToken();
        if ('' === token || undefined === token) {
            next({path: '/login'});
            return;
        }
        checkTokenValid(token).then(res => {
            if (res.ok && store.getters.token.length > 0) {
                next();
            } else {
                throw new Error(res.message);
            }
        }).catch(() => {
            store.dispatch('ClearToken').then(() => {
                next({path: '/login'})
            });
        });
    }
});

router.afterEach(() => {
    NProgress.done();
});

router.addRoutes([...CommonRouter, ...ViewsRouter]);

export default router
