import request from '@/config/axios'

export const generateToken = (user) => request({
    url: '/user_api/auth/token',
    method: 'post',
    data: user
});

export const checkTokenValid = (token) => request({
    url: '/user_api/auth/token/' + token,
    method: 'post'
});
